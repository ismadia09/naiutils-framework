//
//  NAIRoundedButton.swift
//  NAIUtils
//
//  Created by Ismaël Diallo on 11/10/2020.
//  Copyright © 2020 Ismaël Diallo. All rights reserved.
//

import Foundation
import UIKit

open class NAIRoundedButton: UIButton {
    var i = 0
    open override func layoutSubviews() {
        super.layoutSubviews()
        
        //aligner l'image view à droite
        if imageView != nil {
            imageEdgeInsets = UIEdgeInsets(top: 5, left: (bounds.width - 40), bottom: 5, right: 5)
            titleEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: (imageView?.frame.width)!)
        }
    }

    override init(frame: CGRect) {
        super.init(frame:frame)
        self.layer.cornerRadius = self.frame.height / 2

    }
    
    public required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.layer.cornerRadius = self.frame.height / 2
        self.addTarget(self, action: #selector(tapped), for: .touchUpInside)

    }
    
    open func setButtonTitle(title : String, color: UIColor, font : UIFont){
        let attributedKey : [NSAttributedString.Key : Any] = [ NSAttributedString.Key.font: font, NSAttributedString.Key.foregroundColor: color ]
        self.setAttributedTitle(NSAttributedString(string: title, attributes: attributedKey ), for: .normal)
    }
    
    
    @objc open func tapped() {
        NAIHapticUtils.shared.generateHapticWhenButtonPressed()
    }
}
