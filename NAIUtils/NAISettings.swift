//
//  NAISettings.swift
//  NAIUtils
//
//  Created by Ismaël Diallo on 01/01/2020.
//  Copyright © 2020 Ismaël Diallo. All rights reserved.
//

import Foundation
open class NAISettings {
    
    public static var shared = NAISettings()
    var requestBaseUrl = ""
    init() {
    }
    func getHost() -> String {
        return requestBaseUrl
    }
}
