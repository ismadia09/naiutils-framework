//
//  NAIDateUtils.swift
//  NAIUtils
//
//  Created by Ismaël Diallo on 14/05/2021.
//  Copyright © 2021 Ismaël Diallo. All rights reserved.
//

import Foundation


open class NAIDateUtils {
  public static var dateFormat = "yyyy-MM-dd"
  
  public static func getNow() -> Date{
    let currentDate = Date()
    return currentDate
  }
  
  
  public static func getNowString() -> String {
    return formatDate(date: getNow())
  }
  
  public static func formatDate(date : Date, localeIdentifier : String = "fr", dateFormat : String = dateFormat) -> String {
    let dateFormatter = DateFormatter()
    dateFormatter.dateFormat = dateFormat
    dateFormatter.locale = Locale(identifier: localeIdentifier)
    let dateString = dateFormatter.string(from: date)
    return dateString
  }
  
  
  public static func addDays(date : Date, days : Int) -> Date?{
    let wantedDate = Calendar.current.date(byAdding: .day, value: days, to: date)
    return wantedDate
  }
  
    public static func searchDate(date : Date, byAdding : Calendar.Component, count : Int, calendar : Calendar = Calendar.current) -> Date?{
      let wantedDate = calendar.date(byAdding: byAdding, value: count, to: date)
      return wantedDate
    }
  
  public static func dateFromString(date : String, localeIdentifier : String = "fr", dateFormat : String = dateFormat) -> Date?{
    let dateFormatter = DateFormatter()
    dateFormatter.dateFormat = dateFormat
    dateFormatter.locale = Locale(identifier: localeIdentifier)
    let d = dateFormatter.date(from: date)
    return d
  }
    
    public static func daysBetween(start: Date, end: Date) -> Int {
         Calendar.current.dateComponents([.day], from: start, to: end).day!
      }

}
  
