//
//  NAIStringUtils.swift
//  NAIUtils
//
//  Created by Ismaël Diallo on 11/04/2020.
//  Copyright © 2020 Ismaël Diallo. All rights reserved.
//

import Foundation

open class NAIStringUtils {
    public static func isValidEmail(_ email: String) -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        let emailPred = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailPred.evaluate(with: email)
    }
    
    
    
    /**
    * ^                 # start-of-string
    * (?=.*\d)                //should contain at least one digit
    * (?=.*[A-Z])             //should contain at least one upper case
    * [A-Za-z0-9_@./#&+$€-]{4,}         //should contain at least 8 from the mentioned characters
    * $                 # end-of-string
    */
    public static func isValidPassword(_ password: String) -> Bool {
        let passwordRegEx = "^(?=.*\\d)(?=.*[A-Z])[A-Za-z0-9_@./#&+$€-]{4,}$"
        let passwordPred = NSPredicate(format:"SELF MATCHES %@", passwordRegEx)
        return passwordPred.evaluate(with: password)
    }
    
    public static func getJSONString<T>(_ obj : T) -> String? where T : Encodable{
      let encoded = try! JSONEncoder().encode(obj)
      let jsonString = String(data: encoded, encoding: .utf8)
      return jsonString
    }
    
    public static func jsonToDictionary(from text: String) -> [String: Any]? {
      guard let data = text.data(using: .utf8) else { return nil }
      let anyResult = try? JSONSerialization.jsonObject(with: data, options: [])
      return anyResult as? [String: Any]
    }
    
    public static func decodeDictionary<T>(objectType : T.Type, dictionary : [String : Any]) -> T? where T : Codable{
        do {
            let jsonData = try JSONSerialization.data(withJSONObject: dictionary, options: [])
            let jsonString = String(data: jsonData, encoding: String.Encoding.ascii)
            print (jsonString)
            let objectDecoded = try JSONDecoder().decode(objectType, from: jsonData)
            return objectDecoded
        } catch {
            print(error)
        }
        return nil
    }

}
