//
//  NAICustomImageView.swift
//  NAIUtils
//
//  Created by Ismaël Diallo on 08/02/2020.
//  Copyright © 2020 Ismaël Diallo. All rights reserved.
//


import Foundation
import UIKit
import Alamofire

let imageCache = NSCache<AnyObject, AnyObject>()
open class NAICustomImageView : UIImageView {
    var imageUrlString : String?
    let sessionManager = Alamofire.SessionManager.default
    public func downloaded(from url: URL, urlString : String) {
        
        
        
        
        imageUrlString = urlString
//        sessionManager.adapter = OAuth2Handler.sharedInstance
        //Si l'image a deja ete chargée on utilise le cache qui a pour clé l'URL
        if let imageFromCache = imageCache.object(forKey: urlString as AnyObject) as? UIImage{
            DispatchQueue.main.async() {
                self.image = imageFromCache
            }
        }
        //Background thread
        DispatchQueue.global(qos: .background).async {
            self.sessionManager.request(url).validate().responseData { (data) in
                if let data =  data.data {
                    guard
                        let image = UIImage(data: data) else {
                            return
                    }
                    DispatchQueue.main.async() {
                        let imageToCache = image
                        if self.imageUrlString == urlString {
                            self.image = imageToCache
                        }
                        
                        //On ajoute l'image au cache avec comme clé son urlString
                        imageCache.setObject(imageToCache, forKey: urlString as AnyObject)
                    }
                }
            }
        }
        

    }
    
    public func downloaded(from link: String) {
        if link.isValidURL {
            guard let url = URL(string: link) else { return }
            downloaded(from: url, urlString : link)
        }
    }
    
    
    public func decodeFormBase64(string : String){
        imageUrlString = string
        
        //Si l'image a deja ete chargée on utilise le cache qui a pour clé l'URL
        if let imageFromCache = imageCache.object(forKey: string as AnyObject) as? UIImage{
            DispatchQueue.main.async() {
                self.image = imageFromCache
            }
            return
        }
        if let decodedData = Data(base64Encoded: string, options: .ignoreUnknownCharacters) {
            if let image = UIImage(data: decodedData) {
                self.image = image
                let imageToCache = image
                if self.imageUrlString == string {
                    self.image = imageToCache
                }
                
                //On ajoute l'image au cache avec comme clé son urlString
                imageCache.setObject(imageToCache, forKey: string as AnyObject)
            }
           
        }
       
    }
}





