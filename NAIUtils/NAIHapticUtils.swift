//
//  NAIHapticUtils.swift
//  NAIUtils
//
//  Created by Ismaël Diallo on 11/10/2020.
//  Copyright © 2020 Ismaël Diallo. All rights reserved.
//

import Foundation
import UIKit
open class NAIHapticUtils {

//    static let formTitleFontSize : CGFloat = 22
//    static let formTitleFont = FontList.futuraMedium16?.withSize(24)
//
    
    public static let shared = NAIHapticUtils()
    
    
    open func generateHapticWhenScroll(){
        let generator = UIImpactFeedbackGenerator(style: .light)
        generator.impactOccurred()
    }
    
    
    open func generateHapticWhenButtonPressed(){
        let generator = UIImpactFeedbackGenerator(style: .medium)
        generator.impactOccurred()
    }
    
    
    open func generateHapticWhenSuccess(){
        let generator = UINotificationFeedbackGenerator()
        generator.notificationOccurred(.success)
    }
    
    
    open func generateHapticWhenFailure(){
        let generator = UINotificationFeedbackGenerator()
        generator.notificationOccurred(.error)
    }
}
