//
//  NAIAbstractRequest.swift
//  NAIUtils
//
//  Created by Ismaël Diallo on 01/01/2020.
//  Copyright © 2020 Ismaël Diallo. All rights reserved.
//

import Foundation
import Alamofire

open class NAIAbstractRequest{
  
 public enum BodyMediaType : String {
    case JSON = "application/json"
    case FORM_DATA = "application/x-www-form-urlencoded"
  }
  
  let API_BASE_URL = NAISettings.shared.getHost()
  public var sessionManager = Alamofire.SessionManager.default
 

  var accessToken = ""
  var refreshToken = ""

  open var method : HTTPMethod
  open var bodyMediaType : BodyMediaType
  open var endPoint : String
  open var responseType : Decodable.Type?
  open var responseClass : AnyClass?
  open var parameters : Parameters? = [:]
  open var headers : [String : String] = [:]
  open var urlParams : [String : String] = [:]
  open var isDebugLogsEnabled = false
  
  var encoding : ParameterEncoding =  URLEncoding.default
  
  public init(method : HTTPMethod, bodyMediaType : BodyMediaType, parameters : Parameters?, headers : [String : String] ,endPoint : String
    ) {
    self.method = method
    self.bodyMediaType = bodyMediaType
    self.endPoint = API_BASE_URL + endPoint
    self.parameters = parameters
    self.headers = headers
    if (bodyMediaType.rawValue.elementsEqual(BodyMediaType.JSON.rawValue)){
      self.encoding = JSONEncoding(options: [])
    }
    
//    sessionManager.adapter = OAuth2Handler.sharedInstance
//    sessionManager.retrier = OAuth2Handler.sharedInstance
  
  }
  
  
  
 public convenience init(method : HTTPMethod, bodyMediaType : BodyMediaType, parameters : Parameters?, headers : [String : String] ,endPoint : String, responseType : Decodable.Type
      ) {
     self.init(method: method, bodyMediaType: bodyMediaType, parameters: parameters, headers: headers, endPoint: endPoint)
      self.responseType = responseType
     
    }
  
  public convenience init(method : HTTPMethod, bodyMediaType : BodyMediaType, parameters : Parameters?, headers : [String : String] ,endPoint : String, responseClass : AnyClass
    ) {
   self.init(method: method, bodyMediaType: bodyMediaType, parameters: parameters, headers: headers, endPoint: endPoint)
    self.responseClass = responseClass
   
  }
  
  
  public func addUrlParams(key : String, value : String){
    self.urlParams[key] = value
    let urlArray = endPoint.components(separatedBy: "?")
    var url = urlArray[0] + "?"
    var urlParamsString = ""
    for key in urlParams.keys {
      if let value = urlParams[key] {
        urlParamsString += key + "=" + value + "&"
      }
    }
    urlParamsString.removeLast()
    url += urlParamsString
    self.endPoint = url
    
  }
    
  public func doRequest<T>(modelType: T.Type, completion: @escaping (Int?, Decodable?, ErrorResponse?, Error?) -> Void) where T : Decodable{
    self.sessionManager.request(self.endPoint, method: self.method, parameters: self.parameters, encoding: self.encoding, headers: self.headers).validate().responseData { (response) in

      
     
      if self.isDebugLogsEnabled {
            print("###### Request begin ######")
            print("Endpint : " + self.endPoint)
            print(" WS duration  \(response.timeline.totalDuration)")
            print("###### Request end ######")
            if let rdt = response.data, let utf8Text = String(data: rdt, encoding: .utf8)  {
              print(utf8Text)
            }
          }
      guard let responseData = response.data else {
        return
      }
      let statusCode = response.response?.statusCode

      if statusCode == 200 {
        do {
          
          let responseDecoded = try JSONDecoder().decode(modelType, from: responseData)
          print(responseDecoded)
          completion(statusCode,responseDecoded,nil,nil)
        }catch{
          //JSON Decode Error
          print(error)
          DispatchQueue.main.async {
            completion(statusCode,nil,nil,error.self)
          }
        }
      }else{
        do {
          let errorResponse = try JSONDecoder().decode(ErrorResponse.self, from: responseData)
          print(errorResponse)
          
          DispatchQueue.main.async {
            completion(statusCode,nil,errorResponse,nil)
          }
        }catch{
          //JSON Decode Error
          print(error)
          DispatchQueue.main.async {
            completion(statusCode,nil,nil,error.self)
          }
        }
      }
    }
  }
  
 public func doJSONRequest(completion: @escaping (Int?, DataResponse<Any>, ErrorResponse?) -> Void){
    self.sessionManager.request(self.endPoint, method: self.method, parameters: self.parameters, encoding: self.encoding, headers: self.headers).validate().responseJSON { (response) in
      let statusCode = response.response?.statusCode
      //In Order to see logs
        if self.isDebugLogsEnabled {
        print("###### Request begin ######")
        print("Endpint : " + self.endPoint)
        print(" WS duration  \(response.timeline.totalDuration)")
        print("###### Request end ######")
        if let rdt = response.data, let utf8Text = String(data: rdt, encoding: .utf8)  {
          print(utf8Text)
        }
      }
      guard let responseData = response.data else {
        return
      }
      
      do {
        let errorResponse = try JSONDecoder().decode(ErrorResponse.self, from: responseData)
        print(errorResponse)
        DispatchQueue.main.async {
          completion(statusCode,response,errorResponse)
        }
      }catch{
        //JSON Decode Error
        print(error)
        DispatchQueue.main.async {
          completion(statusCode,response,nil)
        }
      }
    }
  }
  
 public func createRequestString(){
    sessionManager.request(endPoint, method: method, parameters: parameters, encoding: encoding, headers: headers).validate().responseString { (response) in
      guard let responseData = response.value else {
        return
      }
      print(responseData)

      
    }
  }
}


public  struct ErrorResponse : Decodable {
    let error : String?
    let message : String
    let hint : String?
    
    private enum CodingKeys : String, CodingKey{
        case error = "error"
        case message = "message"
        case hint = "hint"
    }
    
    init(error : String? ,message : String, hint : String?) {
        self.error = error
        self.message = message
        self.hint = hint
    }
    
    
}
