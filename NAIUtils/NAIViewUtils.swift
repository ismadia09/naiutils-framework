//
//  NAIViewUtils.swift
//  NAIUtils
//
//  Created by Ismaël Diallo on 30/12/2019.
//  Copyright © 2019 Ismaël Diallo. All rights reserved.
//

import Foundation
import UIKit

open class NAIViewUtils {
    
    
    public static func setupTitleNavigationBarItems(navigationController : UINavigationController?,
                                             navigationItem : UINavigationItem,
                                             title : String,
                                             textColor : UIColor,
                                             font : UIFont,
                                             fontSize : CGFloat,
                                             barTintColor : UIColor,
                                             tintColor : UIColor){
        let titleLabel = UILabel(frame: CGRect(x: 0, y: 0, width: 50, height: 34))
        titleLabel.font = font
        titleLabel.text = title
        titleLabel.textColor = textColor
        titleLabel.contentMode = .center
        titleLabel.textAlignment = .center
        navigationItem.titleView = titleLabel
        navigationController?.navigationBar.isTranslucent = false
        navigationController?.navigationBar.barTintColor = barTintColor
        navigationController?.navigationBar.tintColor = tintColor
    }
    
    public static func setupTransparentNavigationBarItems(
            navigationController : UINavigationController?,
            navigationItem : UINavigationItem,
            title : String,
            font : UIFont,
            textColor : UIColor,
            fontSize : CGFloat,
            barTintColor : UIColor,
            tintColor : UIColor){
        let titleLabel = UILabel(frame: CGRect(x: 0, y: 0, width: 50, height: 34))
        titleLabel.font = font
        titleLabel.text = title
        titleLabel.textColor = textColor
        titleLabel.contentMode = .center
        titleLabel.textAlignment = .center
        navigationItem.titleView = titleLabel
        navigationController?.navigationBar.tintColor = tintColor
        navigationController?.navigationBar.barTintColor = barTintColor
         navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        navigationController?.navigationBar.shadowImage = UIImage()
        navigationController?.navigationBar.isTranslucent = true
        navigationController?.view.backgroundColor = .clear
    }
    
    
    public static func dynamicLabelHeightForDetailedView(text : String, font: UIFont) -> CGFloat {
        let screenWidth = UIScreen.main.bounds.width
        let labelWidth = screenWidth - 20
        return text.heightWithConstrainedWidth(width: labelWidth, font: font)
    }
    
    public static func loadViewFromNib(parentView : UIView, name : String) -> Bool {
        let bundle = Bundle(for: type(of: parentView.self))
        let nib = UINib(nibName: name, bundle: bundle)
        if let view = nib.instantiate(withOwner: parentView.self, options: nil).first as? UIView{
            view.frame = parentView.bounds
            view.autoresizingMask = [UIView.AutoresizingMask.flexibleWidth, UIView.AutoresizingMask.flexibleHeight]
            parentView.addSubview(view)
            view.topAnchor.constraint(equalTo: parentView.topAnchor, constant: 0).isActive = true
            view.bottomAnchor.constraint(equalTo: parentView.bottomAnchor, constant: 0).isActive = true
            view.leadingAnchor.constraint(equalTo: parentView.leadingAnchor, constant: 0).isActive = true
            view.trailingAnchor.constraint(equalTo: parentView.trailingAnchor, constant: 0).isActive = true
            view.clipsToBounds = true
            return true
        }
        return false
    }
}

public extension UIView {
    func addSubviews(_ views: UIView...){
        for view in views {
            self.addSubview(view)
        }
    }
    func fillWith(_ view : UIView){
        view.translatesAutoresizingMaskIntoConstraints = false
        view.topAnchor.constraint(equalTo: self.topAnchor).isActive = true
        view.bottomAnchor.constraint(equalTo: self.bottomAnchor).isActive = true
        view.leadingAnchor.constraint(equalTo: self.leadingAnchor).isActive = true
        view.trailingAnchor.constraint(equalTo: self.trailingAnchor).isActive = true
    }

    func setBackgroundGradient(startingColor : UIColor, endingColor : UIColor){
        let gradient = CAGradientLayer()
        gradient.colors = [startingColor.cgColor, endingColor.cgColor]
        gradient.locations = [0.0 , 1.0]
        gradient.startPoint = CGPoint(x: 1.0, y: 1.0)
        gradient.endPoint = CGPoint(x: 0.0, y: 0.0)
        layer.insertSublayer(gradient, at: 0)
        gradient.frame = bounds
    }
    
     func fillSuperview() {
        guard let superview = self.superview else { return }
        translatesAutoresizingMaskIntoConstraints = superview.translatesAutoresizingMaskIntoConstraints
        if translatesAutoresizingMaskIntoConstraints {
            autoresizingMask = [.flexibleWidth, .flexibleHeight]
            frame = superview.bounds
        } else {
            topAnchor.constraint(equalTo: superview.topAnchor).isActive = true
            bottomAnchor.constraint(equalTo: superview.bottomAnchor).isActive = true
            leftAnchor.constraint(equalTo: superview.leftAnchor).isActive = true
            rightAnchor.constraint(equalTo: superview.rightAnchor).isActive = true
        }
    }
    
     func fillSuperview(withPadding padding : CGFloat) {
           guard let superview = self.superview else { return }
           translatesAutoresizingMaskIntoConstraints = superview.translatesAutoresizingMaskIntoConstraints
           if translatesAutoresizingMaskIntoConstraints {
               autoresizingMask = [.flexibleWidth, .flexibleHeight]
               frame = superview.bounds
           } else {
               topAnchor.constraint(equalTo: superview.topAnchor,constant: padding).isActive = true
               bottomAnchor.constraint(equalTo: superview.bottomAnchor, constant: -padding).isActive = true
               leftAnchor.constraint(equalTo: superview.leftAnchor, constant: padding).isActive = true
               rightAnchor.constraint(equalTo: superview.rightAnchor, constant: -padding).isActive = true
           }
       }
}

public extension String {
    func heightWithConstrainedWidth(width: CGFloat, font: UIFont) -> CGFloat {
        let constraintRect = CGSize(width: width, height: .greatestFiniteMagnitude)
        let boundingBox = self.boundingRect(with: constraintRect, options: [.usesLineFragmentOrigin, .usesFontLeading], attributes: [NSAttributedString.Key.font: font], context: nil)
        return boundingBox.height
    }
    var isValidURL: Bool {
        let detector = try! NSDataDetector(types: NSTextCheckingResult.CheckingType.link.rawValue)
        if let match = detector.firstMatch(in: self, options: [], range: NSRange(location: 0, length: self.utf16.count)) {
            // it is a link, if the match covers the whole string
            return match.range.length == self.utf16.count
        } else {
            return false
        }
    }
}


public extension UIApplication {
    
    static func topViewController() -> UIViewController? {
        var topController : UIViewController?
            if var top = shared.keyWindow?.rootViewController {
                while let next = top.presentedViewController {
                    top = next
                }
                topController = top
            }
        return topController
        
    }
}

public enum ImageFormat {
    case png
    case jpeg(CGFloat)
}

public extension UIImage {
    func toBase64(format: ImageFormat) -> String? {
        var imageData: Data?
        
        switch format {
        case .png:
            imageData = self.pngData()
        case .jpeg(let compression):
            imageData = self.jpegData(compressionQuality: compression)
        }
        
        return imageData?.base64EncodedString(options: .lineLength64Characters)
    }
    
    static func base64ToImage(base64: String) -> UIImage {
        var img: UIImage = UIImage()
        if (!base64.isEmpty) {
            if let decodedData = Data(base64Encoded: base64 , options: NSData.Base64DecodingOptions.ignoreUnknownCharacters) {
                let decodedimage = UIImage(data: decodedData)
                img = decodedimage as UIImage? ?? UIImage()
            }
            
        }
        return img
    }
    
    func resized(withPercentage percentage: CGFloat) -> UIImage? {
        let canvas = CGSize(width: size.width * percentage, height: size.height * percentage)
        return UIGraphicsImageRenderer(size: canvas, format: imageRendererFormat).image {
            _ in draw(in: CGRect(origin: .zero, size: canvas))
        }
    }
    func resized(toWidth width: CGFloat) -> UIImage? {
        
        //ceil Arrondi le float
        let canvas = CGSize(width: width, height: CGFloat(ceil(width/size.width * size.height)))
        return UIGraphicsImageRenderer(size: canvas, format: imageRendererFormat).image {
            _ in draw(in: CGRect(origin: .zero, size: canvas))
        }
    }
}

