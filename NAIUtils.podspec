#
#  Be sure to run `pod spec lint NAIUtils.podspec' to ensure this is a
#  valid spec and to remove all comments including this before submitting the spec.
#
#  To learn more about Podspec attributes see https://docs.cocoapods.org/specification.html
#  To see working Podspecs in the CocoaPods repo see https://github.com/CocoaPods/Specs/
#

Pod::Spec.new do |spec|

  # ―――  Spec Metadata  ―――――――――――――――――――――――――――――――――――――――――――――――――――――――――― #
  #
  #  These will help people to find your library, and whilst it
  #  can feel like a chore to fill in it's definitely to your advantage. The
  #  summary should be tweet-length, and the description more in depth.
  #

  spec.name         = "NAIUtils"
  spec.version      = "0.1.4"
  spec.summary      = "bonjour ceci est un test"

  # This description is used to generate tags and improve search results.
  #   * Think: What does it do? Why did you write it? What is the focus?
  #   * Try to keep it short, snappy and to the point.
  #   * Write the description between the DESC delimiters below.
  #   * Finally, don't worry about the indent, CocoaPods strips it!
  spec.description  = "Test, bonjour ceci est un test"

  spec.homepage     = "https://gitlab.com/ismadia09/naiutils-framework.git"
  # spec.screenshots  = "www.example.com/screenshots_1.gif", "www.example.com/screenshots_2.gif"

  spec.license      = "MIT License

Copyright (c) 2020 Diallo Ismaël

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files NAIUtils, to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.
"

  spec.author             = { "Ismaël Diallo" => "ismadia09@gmail.com" }
  spec.social_media_url   = "https://twitter.com/ismadia09"
  spec.source       = { :git => "https://gitlab.com/ismadia09/naiutils-framework.git", :tag => "#{spec.version}" }


  spec.exclude_files = "Classes/Exclude"
  spec.source_files = 'NAIUtils/*.swift'
  spec.dependency 'Alamofire', '~> 4.0'
  spec.dependency 'JWTDecode', '~> 2.2'
  spec.dependency 'SwiftyJSON', '~> 4.0'
  spec.ios.deployment_target  = '10.3'

end
